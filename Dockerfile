FROM sidlors/alpine:openjdk-11
WORKDIR /app
COPY build/libs/tienda-online-0.0.1-SNAPSHOT.jar /app
EXPOSE 8080 8081
CMD ["-jar","/app/tienda-online-0.0.1-SNAPSHOT.jar"]