INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (4250604,'NUTRICION','0','https://youtu.be/ans4gKFjakY','ALOE BETA PIÑA BOTELLA 200 ML                              ',5,33.4,35.26,33.32,31.38,29.45,27.51,37.07,43.0,'Sistema digestivo, Esto se debe a que su contenido en sábila es de gran ayuda para el sistema digestivo. Esta planta es conocida por prevenir enfermedades gástricas e incluso sanar daños producidos en las paredes estomacales. Igualmente, las propiedades desinflamantes del Aloe Vera son altamente beneficioso para el colon.
Sistema inmunológico, Los nutrientes presentes en Aloe Beta hacen que las defensas naturales del cuerpo se vean reforzadas, lo cual ayuda al organismo a mantenerse saludable y de gran manera.
Sistema respiratorio. Al beber esta bebida, ayudamos al cuerpo a mantenerse hidratado y sano, ayudando así a tener una garganta sana. De igual forma, los órganos encargados de la respiración se ven gratamente afectados por este suplemento.
Sistema nervioso, Dado que se trata de un suplemento rico en nutrientes desinflamatorios, el sistema nervioso se ve beneficiado ya que contrarresta los efectos negativos del vertiginoso ritmo de vida del hombre moderno.
Sistema urinario
El componente principal de este suplemento nutricional es el aloe vera, un potente antiinflamatorio. Dado que los riñones, la vejiga y todo el sistema renal suelen inflamarse durante su proceso de filtrado, es natural que la ingesta de Aloe Beta colabore con este importante sistema del cuerpo humano.'),
	 (4310406,'HIDRATANTES Y REFRESCANTES','','','CHIVA COLA SABOR TRADICIONAL PET BOTELLA 600 ML            ',1,13.2,13.93,13.17,12.4,11.64,10.87,14.65,17.0,''),
	 (4850500,'NUTRICION','','','EGO FRUTAS DEL BOSQUE LATA 355 ML (SIX)                    ',26,217.46,229.55,216.94,204.33,191.71,179.1,241.38,280.0,''),
	 (1260600,'NUTRICION','https://i.imgur.com/63XaA7q.png','https://youtu.be/T1P-mg7S5Yw','EGO PLANT, BOTELLA 200 ML                                  ',5,33.4,35.26,33.32,31.38,29.45,27.51,37.07,43.0,'Ego Plant es una bebida hecha con extractos de hierbas que ayudan a la depuración y limpieza del Sistema Renal (Riñones y Vias Urinarias)'),
	 (1578400,'NUTRICION','https://i.imgur.com/qA50vCk.png','https://youtu.be/tVH_mlVLhLI','FIBER’N PLUS SUPREME, CAJA CON 30 SACHETS                  ',66,450.45,475.5,449.37,423.24,397.12,370.99,500.0,580.0,'Es un excelente auxiliar para bajar de peso, ya que ayuda a desechar grasa, colesterol malo y triglicéridos.
Ayuda a facilitar el tránsito intestinal, combate el estreñimiento, por lo que favorece la prevención de cáncer al colon y recto (hemorroides).
Favorece el buen funcionamiento del sistema digestivo, ayuda a la pronta eliminación de los residuos y parásitos.
También ayuda a eliminar, diarrea, colitis, gastritis, ulceras, exceso de acidez, vinagreras, cálculos a la vesícula biliar y el mal aliento.
Elimina el ácido úrico, problemas renales manchas en la cara.
El calcio que contiene ayuda a eliminar problemas del sistema óseo, artritis, osteoporosis, reumatismo, artrosis, la descalcificación que sufren las gestantes.'),
	 (1502208,'NUTRICION LIGERA','https://i.imgur.com/qA50vCk.png','https://youtu.be/tVH_mlVLhLI','FIBER N PLUS CAJA E                                        ',67,450.45,475.5,449.37,423.24,397.12,370.99,500.0,580.0,'Es un excelente auxiliar para bajar de peso, ya que ayuda a desechar grasa, colesterol malo y triglicéridos.
Ayuda a facilitar el tránsito intestinal, combate el estreñimiento, por lo que favorece la prevención de cáncer al colon y recto (hemorroides).
Favorece el buen funcionamiento del sistema digestivo, ayuda a la pronta eliminación de los residuos y parásitos.
También ayuda a eliminar, diarrea, colitis, gastritis, ulceras, exceso de acidez, vinagreras, cálculos a la vesícula biliar y el mal aliento.
Elimina el ácido úrico, problemas renales manchas en la cara.
El calcio que contiene ayuda a eliminar problemas del sistema óseo, artritis, osteoporosis, reumatismo, artrosis, la descalcificación que sufren las gestantes.'),
	 (2075722,'RENDIMIENTO Y DEPORTE','0','https://youtu.be/0lbyzvAG-2s','MAGNUS SUPREME, CAJA CON 30 SACHETS                        ',53,392.2,414.01,391.26,368.51,345.76,323.02,435.34,505.0,'Este suplemento alimenticio posee un alto contenido de enzima QH, componente que aporta nutrición a todo el sistema cardiovascular, fortaleciendo el corazón y todo el sistema cardiaco en general. También proporciona fuerza a todos los músculos del cuerpo.

Otra de las bondades de AQTÚA Supreme es el hecho de ser un gran antioxidante, ayudando así a eliminar los radicales libres del cuerpo y darle salud y fortaleza a la piel.

Es de gran ayuda para mejorar las funciones del sistema circulatorio, por lo que se recomienda este producto a personas que padezcan problemas cardiovasculares.

Al ser rico en L-Carnitina, consumir este suplemento ayudará en gran medida al cuerpo a producir energía a una escala celular, indispensable para realizar todas las actividades del día a día y prevenir problemas cardiovasculares y del corazón.

Al ingerir este poderoso suplemento nutricional, el magnesio ayuda en la producción de energía. La ribosa, carbohidrato importante en la producción del ATP, también complementa la función del magnesio, formando un cóctel enérgico de gran duración. La coenzima QH también colabora con el proceso de eliminación de los radicales libres del cuerpo.'),
	 (2350430,'NUTRICION','','','OMNIPLUS FRUTAS 30 SACH 600 ML                             ',79,543.65,573.88,542.35,510.81,479.28,447.75,603.45,700.0,''),
	 (6379929,'NUTRICION','https://i.imgur.com/s8AA5oO.png','https://youtu.be/GirZh6KFQfw','ONE C MIX PLUS CAJA CON 30 SOBRES 150 GRS                  ',43,333.95,352.52,333.15,313.78,294.41,275.04,370.68,430.0,'El glutation Es el mayor antioxidante endógeno producido por las células, participa directamente en la neutralización de radicales libres y compuestos de oxígeno reactivo, así como en el mantenimiento de los antioxidantes exógenos; por ejemplo, las vitaminas C y E, en sus formas activas, es decir, el glutatión tiene la increible capacidad de regenerarlas. A través de la conjugación directa, desintoxica muchos xenobióticos (compuestos extraños).'),
	 (2603201,'RENDIMIENTO Y DEPORTE','https://i.imgur.com/xjWScUP.png','0','POWER MAKER CAJA C 30 SACHET OMNILIFE                      ',110,683.44,721.44,681.8,642.16,602.52,562.88,758.62,880.0,'Este producto es recomendable para todas aquellas personas que tengan problemas en los riñones a causa de azúcar, deficiencias o piedras formadas dentro de estos mismos.');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (4160100,'NUTRICION','','','AGUA BLU SUPREME, BOTELLA 1.5 L                            ',2,16.22,14.76,13.95,13.14,12.33,11.52,18.0,18.0,''),
	 (4160400,'NUTRICION','','','AGUA BLU SUPREME, BOTELLA 600 ML                           ',1,9.46,8.61,8.14,7.66,7.19,6.72,10.5,10.5,''),
	 (1850610,'NUTRICION','https://i.imgur.com/SEOzb83.png','https://youtu.be/SFDrr3UWYC8','KOLINA, BOTELLA 200 ML                                     ',5,33.4,35.26,33.32,31.38,29.45,27.51,37.07,43.0,'UTIL EN EL TRATAMIENTO DE:
Aerofagia
Distensión abdominal y frecuentes eructos y puede causar dolor 
Disminución de las flatulencias y espasmos abdominales.'),
	 (1945918,'NUTRICION LIGERA','','','MAGILUET CAJA 6 PZAS                                       ',14,112.61,110.67,104.59,98.51,92.43,86.35,125.0,135.0,''),
	 (1940018,'NUTRICION LIGERA','','','MAGIC SILUET, PAQ 6 GALLETAS                               ',14,112.61,110.67,104.59,98.51,92.43,86.35,125.0,135.0,''),
	 (2360401,'NUTRICION','','','OMNIPLUS NARANJA SUPREME, CAJA CON 30 SOBRES               ',78,532.0,561.58,530.72,499.87,469.01,438.16,590.52,685.0,''),
	 (2313801,'NUTRICION','','','OMNIPLUS NARANJA, CAJA CON 30 SOBRES                       ',73,532.0,561.58,530.72,499.87,469.01,438.16,590.52,685.0,''),
	 (5827415,'NUTRICION','','','OMNIVIU SUPREME CAJA CON 30 SACHETS 150 GR                 ',58,473.75,500.09,472.61,445.14,417.66,390.18,525.86,610.0,''),
	 (4250204,'NUTRICION','0','https://youtu.be/ans4gKFjakY','ALOE BETA PIÑA BOTELLA 960ML E                             ',25,151.44,159.86,151.08,142.29,133.51,124.73,168.1,195.0,'Sistema digestivo, Esto se debe a que su contenido en sábila es de gran ayuda para el sistema digestivo. Esta planta es conocida por prevenir enfermedades gástricas e incluso sanar daños producidos en las paredes estomacales. Igualmente, las propiedades desinflamantes del Aloe Vera son altamente beneficioso para el colon.
Sistema inmunológico, Los nutrientes presentes en Aloe Beta hacen que las defensas naturales del cuerpo se vean reforzadas, lo cual ayuda al organismo a mantenerse saludable y de gran manera.
Sistema respiratorio. Al beber esta bebida, ayudamos al cuerpo a mantenerse hidratado y sano, ayudando así a tener una garganta sana. De igual forma, los órganos encargados de la respiración se ven gratamente afectados por este suplemento.
Sistema nervioso, Dado que se trata de un suplemento rico en nutrientes desinflamatorios, el sistema nervioso se ve beneficiado ya que contrarresta los efectos negativos del vertiginoso ritmo de vida del hombre moderno.
Sistema urinario
El componente principal de este suplemento nutricional es el aloe vera, un potente antiinflamatorio. Dado que los riñones, la vejiga y todo el sistema renal suelen inflamarse durante su proceso de filtrado, es natural que la ingesta de Aloe Beta colabore con este importante sistema del cuerpo humano.'),
	 (4278604,'NUTRICION','0','https://youtu.be/ans4gKFjakY','ALOE BETA PIÑA SUPREME, CAJA CON 16 SACHETS                ',57,427.15,450.9,426.12,401.35,376.58,351.8,474.14,550.0,'Sistema digestivo, Esto se debe a que su contenido en sábila es de gran ayuda para el sistema digestivo. Esta planta es conocida por prevenir enfermedades gástricas e incluso sanar daños producidos en las paredes estomacales. Igualmente, las propiedades desinflamantes del Aloe Vera son altamente beneficioso para el colon.
Sistema inmunológico, Los nutrientes presentes en Aloe Beta hacen que las defensas naturales del cuerpo se vean reforzadas, lo cual ayuda al organismo a mantenerse saludable y de gran manera.
Sistema respiratorio. Al beber esta bebida, ayudamos al cuerpo a mantenerse hidratado y sano, ayudando así a tener una garganta sana. De igual forma, los órganos encargados de la respiración se ven gratamente afectados por este suplemento.
Sistema nervioso, Dado que se trata de un suplemento rico en nutrientes desinflamatorios, el sistema nervioso se ve beneficiado ya que contrarresta los efectos negativos del vertiginoso ritmo de vida del hombre moderno.
Sistema urinario
El componente principal de este suplemento nutricional es el aloe vera, un potente antiinflamatorio. Dado que los riñones, la vejiga y todo el sistema renal suelen inflamarse durante su proceso de filtrado, es natural que la ingesta de Aloe Beta colabore con este importante sistema del cuerpo humano.');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (3924734,'NUTRICION','https://i.imgur.com/MYhlbAB.png','https://youtu.be/8fniWeQp0J8','AQTÚA SUPREME, CAJA CON 30 SACHETS                         ',112,698.97,737.83,697.29,656.75,616.21,575.67,775.86,900.0,'HIPERTENSION
CIRCULACIÓN
TRIGLICERIDOS
Los nutrientes que posee participan en la circulación.
Se conoce que la Coenzima Q10 tiene cierta capacidad antioxidante.
Tiene nutrientes que participan en la prevención del accionar de los radicales libres.
La L-cartinina que posee es conocida por participar en el metabolismo de las grasas, por lo que su consumo puede formarte de la dieta de aquellas personas que necesitan un régimen para el control de peso.
Puede ser consumido por deportistas de alta intensidad debido a que tiene nutrientes que participan en el desempeño muscular.
Podemos encontrar nutrientes que son importantes para la piel.
Tiene nutrientes que participan en la reducción de los daños producidos por los radicales libres al cerebro.'),
	 (5177432,'NUTRICION LIGERA','0','https://youtu.be/eqgaRaP4fKs','CAFEZZINO PLUS, CAJA CON 30 SACHETS                        ',51,415.5,438.6,414.5,390.4,366.3,342.21,461.21,535.0,'En conjunto el CAFEZZINO PLUS, favorece aspectos metabólicos implicados en procesos de control de peso corporal.
Contribuye a que el organismo utilice las grasas como energía.El CAFÉ VERDE ORGÁNICO aporta el nutriente ÁCIDO CLOROGÉNICO, que es un potente antioxidante. Este componente es fundamental para la prevención del cáncer, porque protege a las células de los radicales libres.

Tiene otras funciones como inhibir encimas del metabolismo, provocando que se utilicen las grasas almacenadas para producir energía.'),
	 (5177632,'NUTRICION LIGERA','0','https://youtu.be/eqgaRaP4fKs','CAFEZZINO SIN ENDULZAR BOTE E                              ',58,465.98,491.89,464.86,437.83,410.81,383.78,517.24,600.0,'En conjunto el CAFEZZINO PLUS, favorece aspectos metabólicos implicados en procesos de control de peso corporal.
Contribuye a que el organismo utilice las grasas como energía.El CAFÉ VERDE ORGÁNICO aporta el nutriente ÁCIDO CLOROGÉNICO, que es un potente antioxidante. Este componente es fundamental para la prevención del cáncer, porque protege a las células de los radicales libres.

Tiene otras funciones como inhibir encimas del metabolismo, provocando que se utilicen las grasas almacenadas para producir energía.'),
	 (4310506,'HIDRATANTES Y REFRESCANTES','','','CHIVA COLA SABOR TRADICIONAL 6 LATAS 355 ML                ',6,67.57,71.33,67.41,63.49,59.57,55.65,75.0,87.0,''),
	 (4350506,'HIDRATANTES Y REFRESCANTES','','','CHIVA COLA SIN AZÚCAR, NI CALORÍAS 6 LATAS 355 ML          ',6,67.57,71.33,67.41,63.49,59.57,55.65,75.0,87.0,''),
	 (4350406,'HIDRATANTES Y REFRESCANTES','','','CHIVA COLA SIN AZÚCAR, NI CALORÍAS PET BOTELLA 600 ML      ',1,13.2,13.93,13.17,12.4,11.64,10.87,14.65,17.0,''),
	 (4475910,'NUTRICION LIGERA','0','https://youtu.be/oqcfnTnvDTA','DOLCE VITA SUPREME, CAJA CON 30 SACHETS                    ',63,458.22,483.7,457.12,430.54,403.97,377.39,508.62,590.0,'0'),
	 (4432900,'NUTRICION LIGERA','0','https://youtu.be/oqcfnTnvDTA','DOLCE VITA, FRASCO CON 90 TABLETAS                         ',41,283.47,299.23,282.79,266.35,249.91,233.47,314.65,365.0,'0'),
	 (4650609,'RENDIMIENTO Y DEPORTE','https://i.imgur.com/YcCMtr7.png','https://youtu.be/ouoGpFDKmFg','EGO 10 BOTELLA 200 ML                                      ',5,36.11,38.12,36.02,33.93,31.83,29.74,40.08,46.5,''),
	 (4978904,'HIDRATANTES Y REFRESCANTES','0','0','EGO LIFE SUPREME CAJA C/30 SOBRES 480 G                    ',32,240.76,254.15,240.18,226.22,212.25,198.29,267.24,310.0,'0');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (1675700,'NUTRICION','https://i.imgur.com/y17wcVf.png','https://youtu.be/mAkWxTEAiok','ESTOP PLUS SUPREME, CAJA CON 30 SACHETS                    ',102,695.09,733.74,693.42,653.11,612.79,572.48,771.55,895.0,''),
	 (1429653,'NUTRICION','https://i.imgur.com/jLfKeOD.png','https://youtu.be/Gv0itY_xop8','FEM PLUS, CAJA 30 SACHET 195 G                             ',54,431.03,455.0,430.0,405.0,380.0,355.0,478.44,555.0,'Fem Plus de omnilife el producto del que todo el mundo ¡está hablando! tienes problemas Hormonales? o quizás problemas de miomas o quistes poliquisticos. ¿Periodos abundantes o bastante irregulares? Es el resultado de la unión de tres Productos de Omnilife. Son importantes para la salud del aparato reproductor femenino en cualquier etapa de la vida de la mujer.'),
	 (8453000,'NUTRICION','https://i.imgur.com/YYuzcle.png','https://youtu.be/Jos45c8x3Po','FLU-Y BOTE 60 CÁPSULAS                                     ',39,279.59,295.14,278.92,262.7,246.49,230.27,310.34,360.0,'FLU-Y es un complemento alimenticio con extracto de naranja dulce y magnesio. Este extracto se destaca como un ingrediente que favorece y promueve la reducción del estrés, así como generar un estado de descanso. Ideal para personas que diariamente se enfrentan a situaciones estresantes como exceso de volumen y ritmo de trabajo, problemas familiares descontrolada'),
	 (1729752,'NUTRICION','https://i.imgur.com/TBxp5wo.png','0','HOMO PLUS CAJA CON 30 SACHETS 180G                         ',54,431.03,455.0,430.0,405.0,380.0,355.0,478.44,555.0,'0'),
	 (2023500,'RENDIMIENTO Y DEPORTE','0','https://youtu.be/0lbyzvAG-2s','MAGNUS, CAJA CON 30 SOBRES                                 ',50,392.2,414.01,391.26,368.51,345.76,323.02,435.34,505.0,'Este suplemento alimenticio posee un alto contenido de enzima QH, componente que aporta nutrición a todo el sistema cardiovascular, fortaleciendo el corazón y todo el sistema cardiaco en general. También proporciona fuerza a todos los músculos del cuerpo.

Otra de las bondades de AQTÚA Supreme es el hecho de ser un gran antioxidante, ayudando así a eliminar los radicales libres del cuerpo y darle salud y fortaleza a la piel.

Es de gran ayuda para mejorar las funciones del sistema circulatorio, por lo que se recomienda este producto a personas que padezcan problemas cardiovasculares.

Al ser rico en L-Carnitina, consumir este suplemento ayudará en gran medida al cuerpo a producir energía a una escala celular, indispensable para realizar todas las actividades del día a día y prevenir problemas cardiovasculares y del corazón.

Al ingerir este poderoso suplemento nutricional, el magnesio ayuda en la producción de energía. La ribosa, carbohidrato importante en la producción del ATP, también complementa la función del magnesio, formando un cóctel enérgico de gran duración. La coenzima QH también colabora con el proceso de eliminación de los radicales libres del cuerpo.'),
	 (2050613,'NUTRICION','','','MAGNUS MANZANA BOTELLA 200 ML                              ',5,36.11,38.12,36.02,33.93,31.83,29.74,40.08,46.5,''),
	 (5310513,'HIDRATANTES Y REFRESCANTES','','','OMNILIFE FX MANZANA, PAQ. CON 6 LATAS                      ',11,72.23,76.25,72.06,67.87,63.68,59.49,80.18,93.0,''),
	 (5310501,'HIDRATANTES Y REFRESCANTES','','','OMNILIFE FX NARANJA, PAQ. CON 6 LATAS                      ',11,72.23,76.25,72.06,67.87,63.68,59.49,80.18,93.0,''),
	 (3659152,'NUTRICION','0','https://youtu.be/UrmOhkuVwIs','OMNILIFE IQU LATA 355 ML (SIX PACK)                        ',40,326.19,344.33,325.41,306.49,287.57,268.65,362.07,420.0,'0'),
	 (6722800,'NUTRICION','https://i.imgur.com/0vgKfsT.png','https://youtu.be/rSHir4Kf9SE','OMNILIFE PROBIOTIC CAJA 30 SOBRES SACHET 150G              ',62,454.33,479.59,453.24,426.89,400.54,374.19,504.31,585.0,'0');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (5479243,'RENDIMIENTO Y DEPORTE','https://i.imgur.com/SeWkPXZ.png','https://youtu.be/hlm7R93gueA','OMNILIFE PUMP SUPREME CAJA 12 SOBRES                       ',69,431.03,455.0,430.0,405.0,380.0,355.0,478.44,555.0,'DESPUES DE CADA ENTRENAMIENTO le proveo a mi cuerpo una proteína de fácil absorción, sin grasa, sin lactosa, sin caseína.
Hoy te hablaré de la proteína que yo tomo: el Pump. Una proteína incluso que es adecuada para veganos pues no contiene derivados de origen animal. Acompáñame y descubre porque elegí esta proteína y la razón por la que forma parte de mi nutrición deportiva.

EL PUMP contiene proteína aislada de soya.
Pero a que me refiero con “AISLADO DE PROTEINA” 
Se trata de una forma altamente refinada de proteína de soya con un contenido proteico mínimo del 90%. Se elabora a partir de soya que ya ha sido desgrasada, además a través de este proceso de aislamiento se eliminan sus componentes no-proteicos, como son grasas, carbohidratos'),
	 (7170943,'NUTRICION LIGERA','0','https://youtu.be/uk6C-zdyH-c','OMNILIFE SHAKE SUPREME COOKIES & CREAM, CAJA CON 12 SOBRES ',76,486.49,442.71,418.38,394.06,369.73,345.41,540.0,540.0,'Es una malteada endulzada con Stevia sin ningún elemento animal, no contiene lactosa. Esta diseñada para sustituir la Cena o el desayuno, porque contiene proteínas, vitaminas, fibras, antioxidantes, minerales, y muchos otros nutrientes que tu cuerpo necesita. ¿Qué beneficios tienes al tomarla? ¿Por qué te ayuda a la perdida de peso? Es decir, a esa grasa corporal indeseable. ¿Qué contiene cada sobre de malteada Shake SUPREME y por qué puede sustituir tu cena o desayuno si estas buscando no saltarte alguno de estos alimentos o bien estas buscando perder peso saludablemente o mas bien, esa grasa que esta almacenando tu cuerpo? Debes recordar que tu cuerpo puede acumular grasa por muchas razones, pero hay dos básicas: • Por comer mas calorías de las que tu cuerpo necesita • Por generar un desequilibrio alimentario, es decir, saltarte comidas de manera frecuente.'),
	 (7170921,'NUTRICION LIGERA','0','https://youtu.be/uk6C-zdyH-c','OMNILIFE SHAKE SUPREME FRESA SILVESTRE, CAJA CON 12 SOBRES ',76,486.49,442.71,418.38,394.06,369.73,345.41,540.0,540.0,'Es una malteada endulzada con Stevia sin ningún elemento animal, no contiene lactosa. Esta diseñada para sustituir la Cena o el desayuno, porque contiene proteínas, vitaminas, fibras, antioxidantes, minerales, y muchos otros nutrientes que tu cuerpo necesita. ¿Qué beneficios tienes al tomarla? ¿Por qué te ayuda a la perdida de peso? Es decir, a esa grasa corporal indeseable. ¿Qué contiene cada sobre de malteada Shake SUPREME y por qué puede sustituir tu cena o desayuno si estas buscando no saltarte alguno de estos alimentos o bien estas buscando perder peso saludablemente o mas bien, esa grasa que esta almacenando tu cuerpo? Debes recordar que tu cuerpo puede acumular grasa por muchas razones, pero hay dos básicas: • Por comer mas calorías de las que tu cuerpo necesita • Por generar un desequilibrio alimentario, es decir, saltarte comidas de manera frecuente.'),
	 (7170916,'NUTRICION LIGERA','0','https://youtu.be/uk6C-zdyH-c','OMNILIFE SHAKE SUPREME VAINILLA MAPLE, CAJA CON 12 SOBRES  ',76,486.49,442.71,418.38,394.06,369.73,345.41,540.0,540.0,'Es una malteada endulzada con Stevia sin ningún elemento animal, no contiene lactosa. Esta diseñada para sustituir la Cena o el desayuno, porque contiene proteínas, vitaminas, fibras, antioxidantes, minerales, y muchos otros nutrientes que tu cuerpo necesita. ¿Qué beneficios tienes al tomarla? ¿Por qué te ayuda a la perdida de peso? Es decir, a esa grasa corporal indeseable. ¿Qué contiene cada sobre de malteada Shake SUPREME y por qué puede sustituir tu cena o desayuno si estas buscando no saltarte alguno de estos alimentos o bien estas buscando perder peso saludablemente o mas bien, esa grasa que esta almacenando tu cuerpo? Debes recordar que tu cuerpo puede acumular grasa por muchas razones, pero hay dos básicas: • Por comer mas calorías de las que tu cuerpo necesita • Por generar un desequilibrio alimentario, es decir, saltarte comidas de manera frecuente.'),
	 (3322200,'NUTRICION LIGERA','','','OMNILIFE TMGN COFFEE CAJA C/30 SACHETS                     ',39,306.77,323.83,306.03,288.24,270.45,252.66,340.51,395.0,''),
	 (2923200,'NUTRICION LIGERA','','','OMNILIFE TMGN COFFEE DE OLLA CAJA C/30 SACHETS             ',38,306.77,323.83,306.03,288.24,270.45,252.66,340.51,395.0,''),
	 (5025516,'NUTRICION','https://i.imgur.com/97ei4pM.png','https://youtu.be/0XdaNvCsUw4','OMNILIFE VKIDS, DOYPACK 400 G                              ',71,465.98,491.89,464.86,437.83,410.81,383.78,517.24,600.0,'diseñado para niños, que complementa el aporte diario de vitaminas, minerales y ácidos grasos esenciales necesarios para la actividad y desarrollo saludable durante esta etapa. Contiene DHA, un ácido graso esencial que pertenece a la familia del omega 3. El DHA se considera de suma importancia para el desarrollo neurológico y el buen funcionamiento visual del niño. Vkids Omnilife para niños contiene betaglutanos que participan en el fortalecimiento de las defensas del organismo los cuales ayudan a combatir bacterias y a tener una respuesta más favorable frente a infecciones'),
	 (6571959,'NUTRICION','0','https://youtu.be/xPDNAzOoJ0o','OMNIOST CAJA C/30 SOBRES 450 GRS                           ',57,411.62,434.51,410.63,386.76,362.88,339.01,456.9,530.0,'Es un Batido con sabor a plátano, debido a su contenido de calcio, vitamina D y K2 ayuda en el desarrollo de huesos y dientes fuertes. Ayuda a la nutrición del cuerpo.'),
	 (2350330,'NUTRICION','https://i.imgur.com/bgijtxZ.png','0','OMNIPLUS FRUTAS E BOTELLA 940 ML                           ',82,539.76,569.77,538.46,507.16,475.85,444.55,599.13,695.0,''),
	 (6305929,'NUTRICION','','','ONE C MIX, CAJA C/30 SACHETS E                             ',44,345.6,364.82,344.77,324.73,304.68,284.64,383.62,445.0,'');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (2505711,'NUTRICION','https://i.imgur.com/ChAEevH.png','https://youtu.be/jJJOxaoHXsM','OPTIMUS CAJA 30 SACHETS                                    ',61,403.85,426.3,402.88,379.46,356.03,332.61,448.27,520.0,'Nutrición para el cerebro. Limpienza de Higado. hepaprotector. La colina es un nutriente esencial y fundamental para el desarrollo del sistema nervioso. Su aporte permite mejorar las funciones cognitivas, entre otros beneficios. Entre la variedad de vitaminas y minerales que necesitamos ingerir diariamente se encuentra la colina. Es considerada una vitamina del grupo B, que no suele ser tan conocida como las demás, pero es importante obtenerla a través de la alimentación, ya que es un nutriente esencial.'),
	 (2550614,'NUTRICION','','','OPTIMUS JAMAICA BOTELLA 200 ML                             ',5,33.4,35.26,33.32,31.38,29.45,27.51,37.07,43.0,''),
	 (3000730,'NUTRICION','https://i.imgur.com/QSVQxzY.png','https://youtu.be/mjDENPcYjHw','STARBIEN, CAJA CON 30 SOBRES                               ',53,349.49,368.92,348.65,328.38,308.11,287.84,387.93,450.0,'Si quieres reducir tu nivel de estrés, si buscas un aliado contra la depresión, si combatir la fatiga, Si quieres quedar embarazada, Si deseas tener mas energía vital y sexual, para la prevención del cáncer, para regenerar tejidos, para mejorar tu piel y mucho más.'),
	 (3179518,'NUTRICION','0','https://youtu.be/s9LRlcLE2ys','SUPER MIX SUPREME CHOCOLATE, CAJA 510 G                    ',76,508.7,536.98,507.48,477.97,448.47,418.97,564.66,655.0,'¿Te gusta agregar un licuado o batido o maltadea para completar tu nutrición diaría?
Hoy te hablaré del SUPERMIX o también llamada MALTEADA OML en Estados Unidos y el porqué es un aliado importante en la nutrición de niños, adolescentes o adultos.
Contiene:
CONTIENE INULINA DE AGAVE
Contiene ácidos grasos DHA
PROTEINA DE SOYA
VITAMINAS DEL COMPLEJO B
ANTIOXIDANTES COMO VITAMINA A, C Y E
VITAMINA K Y D
MINERALES Y MUCHO MAS'),
	 (3178516,'NUTRICION','1','https://youtu.be/s9LRlcLE2ys','SUPER MIX SUPREME VAINILLA, CAJA CON 30 SACHETS            ',76,508.7,536.98,507.48,477.97,448.47,418.97,564.66,655.0,'¿Te gusta agregar un licuado o batido o maltadea para completar tu nutrición diaría?
Hoy te hablaré del SUPERMIX o también llamada MALTEADA OML en Estados Unidos y el porqué es un aliado importante en la nutrición de niños, adolescentes o adultos.
Contiene:
CONTIENE INULINA DE AGAVE
Contiene ácidos grasos DHA
PROTEINA DE SOYA
VITAMINAS DEL COMPLEJO B
ANTIOXIDANTES COMO VITAMINA A, C Y E
VITAMINA K Y D
MINERALES Y MUCHO MAS'),
	 (3505605,'NUTRICION LIGERA','https://i.imgur.com/m9bvLBb.png','https://youtu.be/_DPhtaNPMpk','TEATINO LIMON, CAJA C/30 SACHETS E                         ',55,411.62,434.51,410.63,386.76,362.88,339.01,456.9,530.0,'0'),
	 (3575609,'NUTRICION LIGERA','0','0','TEATINO MARACUYÁ SUPREME, CAJA CON 30 SACHETS              ',92,563.06,594.37,561.71,529.05,496.39,463.74,625.0,725.0,'0'),
	 (3304732,'NUTRICION LIGERA','','','TMGN COFFEE CAJA EVOLUCION                                 ',39,306.77,323.83,306.03,288.24,270.45,252.66,340.51,395.0,''),
	 (3705934,'RENDIMIENTO Y DEPORTE','https://i.imgur.com/xjWScUP.png','0','UNDU, CAJA C/30 SACHETS',101,632.96,668.15,631.44,594.73,558.02,521.31,702.59,815.0,'Este producto es recomendable para todas aquellas personas que tengan problemas en los riñones a causa de azúcar, deficiencias o piedras formadas dentro de estos mismos.'),
	 (3875700,'NUTRICION','https://i.imgur.com/QwfaVB4.png','0','UZO SUPREME, CAJA CON 30 SACHETS                           ',166,931.97,983.79,929.73,875.68,821.62,767.57,1034.49,1200.0,'');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (9999979,'MATERIAL DE APOYO','','','KIT NUTRICIONAL OMNILIFE 2022                              ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,550.0,''),
	 (9999980,'MATERIAL DE APOYO','','','BOLSA REUTILIZABLE OMNILIFE ELIGE PROSPERAR                ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,35.0,''),
	 (9999981,'MATERIAL DE APOYO','','','CUADERNO MORADO OMNILIFE                                   ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,79.0,''),
	 (9999982,'MATERIAL DE APOYO','','','GUIA DE PRODUCTOS OMNILIFE Vol. 4                          ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,12.0,''),
	 (9999983,'MATERIAL DE APOYO','','','LIBRETA OMNILIFE 30 AÑOS                                   ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,79.0,''),
	 (9999984,'MATERIAL DE APOYO','','','MALETIN OMNILIFE                            ¡NUEVO!        ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,400.0,''),
	 (9999985,'MATERIAL DE APOYO','','','OM MAGAZINE 38 MX                                          ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,50.0,''),
	 (9999986,'MATERIAL DE APOYO','','','PIN KIT NUTRICIONAL                                        ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,35.0,''),
	 (9999987,'MATERIAL DE APOYO','','','PLUMA 3O AÑOS OMNILIFE                                     ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,25.0,''),
	 (9999988,'MATERIAL DE APOYO','','','REPOSICIÓN DE CONTRATO Y CONSTANCIA                        ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,5.0,'');
INSERT INTO public.precios2023 (codigo,tipo,imagen,url_video,descripcion,pts,plista,desc20,desc25,desc30,desc35,desc40,pintegrado,psugerido,beneficio) VALUES
	 (9999989,'MATERIAL DE APOYO','','','TERMO CLASSIC OMNILIFE 2022                                ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,210.0,''),
	 (9999990,'MATERIAL DE APOYO','','','TERMO METALICO OMNILIFE                                    ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,125.0,''),
	 (9999991,'MATERIAL DE APOYO','','','TERMO METALICO 30 AÑOS                                     ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,125.0,''),
	 (9999992,'MATERIAL DE APOYO','','','TERMO OMNILIFE LASER                    ¡NUEVO!            ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,150.0,''),
	 (9999993,'MATERIAL DE APOYO','','','TERMO SPORTMIXER OMNILIFE                                  ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,285.0,''),
	 (9999994,'MATERIAL DE APOYO','','','TERMO SPORTMIXER 30 AÑOS                                   ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,270.0,''),
	 (9999995,'MATERIAL DE APOYO','','','PLAYERA POLO NYLON OMNILIFE H Y M (TALLAS CH, M, G)        ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,420.0,''),
	 (9999996,'MATERIAL DE APOYO','','','CHALECO CAPITONADO OMNILIFE H Y M (TALLAS CH, M, G)        ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,670.0,''),
	 (9999997,'MATERIAL DE APOYO','','','CHAMARRA CAPITONADA OMNILIFE H Y M (TALLAS CH, M, G)       ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,730.0,''),
	 (9999998,'MATERIAL DE APOYO','','','CHAMARRA ROMPEVIENTOS OMNILIFE (TALLAS CH, M, G)           ',0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,740.0,'');
