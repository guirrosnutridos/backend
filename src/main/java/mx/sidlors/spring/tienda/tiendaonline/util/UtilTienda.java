package mx.sidlors.spring.tienda.tiendaonline.util;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Set;

public class UtilTienda {

    private UtilTienda(){}

    public static void copyProperties2(Object src, Object trg, Set<String> props) {
        String[] excludedProperties =
                Arrays.stream( BeanUtils.getPropertyDescriptors(src.getClass()))
                        .map( PropertyDescriptor::getName)
                        .filter(name -> !props.contains(name))
                        .toArray(String[]::new);

        BeanUtils.copyProperties(src, trg, excludedProperties);
    }
}
