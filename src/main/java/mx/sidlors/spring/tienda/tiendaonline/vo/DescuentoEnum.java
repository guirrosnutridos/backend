package mx.sidlors.spring.tienda.tiendaonline.vo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public enum DescuentoEnum {

    PORCIENTO20(),
    PORCIENTO25(),
    PORCIENTO30(),
    PORCIENTO35(),
    PORCIENTO40();



    @JsonCreator
    public static DescuentoEnum fromValue( String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return DescuentoEnum.valueOf(name.toLowerCase ());
    }
}
