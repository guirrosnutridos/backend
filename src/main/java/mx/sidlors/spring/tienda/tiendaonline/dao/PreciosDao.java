package mx.sidlors.spring.tienda.tiendaonline.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.sidlors.spring.tienda.tiendaonline.entity.Precio;

@Repository
public interface PreciosDao extends JpaRepository<Precio, Integer>{

}