package mx.sidlors.spring.tienda.tiendaonline.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Data Model for Error Response.
 */
@Data
public class ErrorResponse implements Serializable {

    /**
     * The constant serialVersionUID.
     */
    private static final long serialVersionUID = -7846773639244185780L;

    /**
     * The Code.
     */
    private String code;

    /**
     * The Title.
     */
    private String title;

    /**
     * The Id.
     */
    private String id;

    /**
     * The Links.
     */
    private String link;

    /**
     * The Detail.
     */
    private String detail;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
