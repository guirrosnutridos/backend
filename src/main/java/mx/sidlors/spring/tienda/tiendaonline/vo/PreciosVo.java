package mx.sidlors.spring.tienda.tiendaonline.vo;

public class PreciosVo {

	private Integer codigo ;

	private String descripcion ;

	private Integer pts ;

	private Long pLista ;

	private Long desc20 ;

	private Long desc25 ;

	private Long desc30 ;

	private Long desc35 ;

	private Long desc40 ;

	private Long pintegrado ;

	private Long psugerido ;

	private String tipo ;

	private String imagen ;

	private String urlVideo ;

	private String beneficio ;

	public PreciosVo(){
		super();
	}

	public Integer getCodigo(){
		return this.codigo;
	}

	public void setCodigo(Integer codigo){
		this.codigo = codigo;
	}

	public String getDescripcion(){
		return this.descripcion;
	}

	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

	public Integer getPts(){
		return this.pts;
	}

	public void setPts(Integer pts){
		this.pts = pts;
	}

	public Long getPLista(){
		return this.pLista;
	}

	public void setPLista(Long pLista){
		this.pLista = pLista;
	}

	public Long getDesc20(){
		return this.desc20;
	}

	public void setDesc20(Long desc20){
		this.desc20 = desc20;
	}

	public Long getDesc25(){
		return this.desc25;
	}

	public void setDesc25(Long desc25){
		this.desc25 = desc25;
	}

	public Long getDesc30(){
		return this.desc30;
	}

	public void setDesc30(Long desc30){
		this.desc30 = desc30;
	}

	public Long getDesc35(){
		return this.desc35;
	}

	public void setDesc35(Long desc35){
		this.desc35 = desc35;
	}

	public Long getDesc40(){
		return this.desc40;
	}

	public void setDesc40(Long desc40){
		this.desc40 = desc40;
	}

	public Long getPintegrado(){
		return this.pintegrado;
	}

	public void setPintegrado(Long pintegrado){
		this.pintegrado = pintegrado;
	}

	public Long getPsugerido(){
		return this.psugerido;
	}

	public void setPsugerido(Long psugerido){
		this.psugerido = psugerido;
	}

	public String getTipo(){
		return this.tipo;
	}

	public void setTipo(String tipo){
		this.tipo = tipo;
	}

	public String getImagen(){
		return this.imagen;
	}

	public void setImagen(String imagen){
		this.imagen = imagen;
	}

	public String getUrlVideo(){
		return this.urlVideo;
	}

	public void setUrlVideo(String urlVideo){
		this.urlVideo = urlVideo;
	}

	public String getBeneficio(){
		return this.beneficio;
	}

	public void setBeneficio(String beneficio){
		this.beneficio = beneficio;
	}

}