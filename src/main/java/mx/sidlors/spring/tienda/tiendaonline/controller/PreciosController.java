package mx.sidlors.spring.tienda.tiendaonline.controller;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import mx.sidlors.spring.tienda.tiendaonline.exception.InvalidArgumentException;
import mx.sidlors.spring.tienda.tiendaonline.vo.DescuentoEnum;
import mx.sidlors.spring.tienda.tiendaonline.vo.PreciosVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mx.sidlors.spring.tienda.tiendaonline.service.PreciosService;
import mx.sidlors.spring.tienda.tiendaonline.vo.PreciosVo;

@RestController
public class PreciosController {

	@Autowired
	PreciosService preciosService;

	@PostMapping("precios")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void save(PreciosVo preciosVo) {
		preciosService.save(preciosVo);
	}

	@GetMapping("precios/{id}")
	public PreciosVo get( @PathVariable Integer id) {
		return preciosService.get(id);
	}

	@GetMapping("precios")
	public List<PreciosVo>  getAll() {
		return preciosService.getAll();
	}
	
	@PutMapping("precios")
	public void update(PreciosVo preciosVo) {
		preciosService.update(preciosVo);
	}

	@DeleteMapping("precios/{id}")
	public void delete( @PathVariable Integer id) {
		preciosService.delete(id);
	}
	
    @GetMapping("precios/descuento/{id}")
    @Operation(summary = "Get a Price por porcentaje de descuento by id")
	@ApiResponses(value = {
	  @ApiResponse(responseCode = "200", description = "Found the Price",
		content = { @Content(mediaType = "application/json")}),
	  @ApiResponse(responseCode = "400", description = "Invalid id supplied", 
		content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Price not found", 
		content = @Content) })
	public PreciosVo precioConDescuento( @Parameter DescuentoEnum descuento, @Parameter Integer id) throws InvalidArgumentException {
		return preciosService.getDescuento(descuento,id);
	}

	@GetMapping("precios/sugerido/{id}")
    @Operation(summary = "Get a Price by descuento and  id")
	@ApiResponses(value = {
	  @ApiResponse(responseCode = "200", description = "Found the Price",
		content = { @Content(mediaType = "application/json")}),
	  @ApiResponse(responseCode = "400", description = "Invalid id supplied", 
		content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Price not found", 
		content = @Content) })
	public PreciosVo precioPublico(@Parameter Integer id) throws InvalidArgumentException {
		return preciosService.getPrecioPublico(id);
	}
	

	@ExceptionHandler(EntityExistsException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public String handleEntityExistsException(EntityExistsException e) {
	    return e.getMessage();
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String handleEntityNotFoundException(EntityNotFoundException e) {
	    return e.getMessage();
	}
}
