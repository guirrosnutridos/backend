package mx.sidlors.spring.tienda.tiendaonline.service.impl;

import java.util.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import static mx.sidlors.spring.tienda.tiendaonline.util.UtilTienda.copyProperties2;
import mx.sidlors.spring.tienda.tiendaonline.vo.DescuentoEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sidlors.spring.tienda.tiendaonline.dao.PreciosDao;
import mx.sidlors.spring.tienda.tiendaonline.entity.Precio;
import mx.sidlors.spring.tienda.tiendaonline.exception.InvalidArgumentException;
import mx.sidlors.spring.tienda.tiendaonline.service.PreciosService;
import mx.sidlors.spring.tienda.tiendaonline.vo.PreciosVo;

@Service
public class PreciosServiceImpl implements PreciosService {

	@Autowired
    PreciosDao preciosDao;

	@Override
	@Transactional
	public void save(PreciosVo preciosVo) {
		Integer id = preciosVo.getCodigo();
		Boolean objectAlreadyExists= preciosDao.existsById(id);
		if(!objectAlreadyExists) {
			Precio precio = new Precio();
			BeanUtils.copyProperties(preciosVo, precio);
			preciosDao.save(precio);
		}else {
			throw new EntityExistsException();
		}
		
	}
	
	@Override
	@Transactional
	public void update(PreciosVo preciosVo) {
		Integer id = preciosVo.getCodigo();
		Boolean objectExists= preciosDao.existsById(id);
		if(objectExists) {
			Precio precio = new Precio();
			BeanUtils.copyProperties(preciosVo, precio);
			preciosDao.save(precio);
		}else {
			throw new EntityNotFoundException();
		}
	}

	@Override
	@Transactional
	public void delete(Integer id) {
		Boolean objectExists= preciosDao.existsById(id);
		if(objectExists) {
			preciosDao.deleteById(id);
		}else {
			throw new EntityNotFoundException();
		}
	}

	@Override
	@Transactional
	public PreciosVo get(Integer id) {
		Optional<Precio> precios2023Optional = preciosDao.findById(id);
		PreciosVo preciosVo =null;
		if(precios2023Optional.isPresent()) {
			preciosVo = new PreciosVo();
			BeanUtils.copyProperties(precios2023Optional.get(), preciosVo);
		}else {
			throw new EntityNotFoundException();
		}
		
		return preciosVo;
	}

	@Override
	@Transactional
	public List<PreciosVo> getAll() {
		List<Precio> precioList = preciosDao.findAll();
		List<PreciosVo> preciosVoList = new ArrayList<>();
		if (!precioList.isEmpty()) {
			for (Precio precio : precioList) {
				PreciosVo preciosVo = new PreciosVo();
				BeanUtils.copyProperties(precio, preciosVo);
				preciosVoList.add(preciosVo);
			}
		}
		return preciosVoList;
	}

	@Override
	public PreciosVo getDescuento( DescuentoEnum descuento, Integer id) throws InvalidArgumentException {

		//Boolean objectAlreadyExists= preciosDao.existsById(id);
		Optional<Precio> precios2023Optional = preciosDao.findById(id);
		Set<String> propeties=new HashSet<String> (  );
		propeties.add ( "codigo" );
		propeties.add ( "descripcion" );
		propeties.add ( "beneficio" );
		propeties.add ( "tipo" );
		propeties.add ( "imagen" );
		propeties.add ( "urlVideo" );
		propeties.add ( "pts" );
		propeties.add ( "psugerido" );
		if(precios2023Optional.isPresent()) {
			PreciosVo preciosVo = new PreciosVo();
			switch (descuento){
				case PORCIENTO20:
					propeties.add ( "desc20" );break;
				case PORCIENTO25:
					propeties.add ( "desc25" );break;
				case PORCIENTO30:
					propeties.add ( "desc30" );break;
				case PORCIENTO35:
					propeties.add ( "desc35" );break;
				case PORCIENTO40:
					propeties.add ( "desc40" );break;
			}
			copyProperties2(precios2023Optional.get (), preciosVo, propeties);
			return preciosVo;
		}else {
			throw new EntityNotFoundException();
		}
		
	}

	@Override
	public PreciosVo getPrecioPublico(Integer id) throws InvalidArgumentException {
		// TODO Auto-generated method stub
		throw new InvalidArgumentException("not implemented yet");
	}

}

