package mx.sidlors.spring.tienda.tiendaonline.service;

import java.util.List;

import mx.sidlors.spring.tienda.tiendaonline.exception.InvalidArgumentException;
import mx.sidlors.spring.tienda.tiendaonline.vo.DescuentoEnum;
import mx.sidlors.spring.tienda.tiendaonline.vo.PreciosVo;

public interface PreciosService {

	void save(PreciosVo preciosVo) ;
	void update(PreciosVo preciosVo) ;
	void delete(Integer id);
	PreciosVo get(Integer id);
	List<PreciosVo> getAll();

	PreciosVo getDescuento( DescuentoEnum descuento, Integer id) throws InvalidArgumentException;
    PreciosVo getPrecioPublico(Integer id) throws InvalidArgumentException;
}

